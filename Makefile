# install location
PREFIX = /usr/local

CB_SOURCE = troubleshoot.sh
ALERT_SOURCE = alert.sh
ALERT_CONF = alert.conf

CB = cb-troubleshoot
ALERT = cb-alert

$(CB) : $(CB_SOURCE) $(ALERT)
	cp $(CB_SOURCE) $(CB)

$(ALERT) :  $(ALERT_SOURCE) $(ALERT_CONF)
	cp $(ALERT_SOURCE) $(ALERT)

clean :
	rm -f $(CB) $(ALERT)

install : $(CB) $(ALERT)
	mkdir -p $(PREFIX)/bin/
	mkdir -p $(PREFIX)/etc/cb-troubleshoot

	# cb-troubleshoot
	cp $(CB) $(PREFIX)/bin/$(CB)
	chmod 755 $(PREFIX)/bin/$(CB)

	# cb-alert
	cp $(ALERT) $(PREFIX)/bin/$(ALERT)
	chmod 755 $(PREFIX)/bin/$(ALERT)

	# alert.conf
	cp $(ALERT_CONF) $(PREFIX)/etc/cb-troubleshoot/$(ALERT_CONF)
	chmod 755 $(PREFIX)/etc/cb-troubleshoot/$(ALERT_CONF)

uninstall :
	rm -f $(PREFIX)/bin/$(CB)
	rm -f $(PREFIX)/bin/$(ALERT)
	rm -rf $(PREFIX)/etc/cb-troubleshoot/$(ALERT_CONF)
	rm -rf $(PREFIX)/etc/cb-troubleshoot

.PHONY : clean install uninstall
