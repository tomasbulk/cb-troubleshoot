#!/bin/bash
#
#     Copyright (C) 2020  Tomas Bulk
#     email:    tomasbulk@gmail.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
#   Simply utility designed for clearing space on CarbonBlack
#   WARNING: this tool is currently untested.

#####################################

check_root () {
	if [[ $EUID -ne 0 ]]; then			# this function checks if the program EUID is 0
		echo "This script must be run as root" 	# as root owns the EUID of 0.
		exit 1					# this will exit the script if not root
	fi
}

warning () {
	cat <<EOF

	WARNING: This tool is not a toy.
	If you don't know what you're doing, maintain a backup of all CarbonBlack files.
	Furthermore: This tool is UNTESTED!! Use at your own risk.
EOF
sleep 3
}

copyright_notice () {
	cat <<EOF
    cb-troubleshoot  Copyright (C) 2020  Tomas Bulk
    This program comes with ABSOLUTELY NO WARRANTY; for details type "${0##*/} -c".
    This is free software, and you are welcome to redistribute it
    under certain conditions; type "${0##*/} -c" for details.

EOF
}

usage () {

	cat <<EOF
	Simple utility designed for clearing space on Carbon Black
	Usage: ${0##*/} -{s,d}
	Options:

	-s	Check the status of the CB partitions
	-d	Clean the CB partitions (Dangerous)
	-c	Copyright Notice
	-h 	Print this dialogue


EOF
exit 1
}

clean_cb () {
	cat <<EOF

Please determine how you'd like to clean:
	1) hprof:	Remove old hprof files.
	2) cbdiag:	Remove old cbdiag files.
	3) clean-var:	Clean /var directory with "yum clean all"
	4) modules:	Remove cb_module files and cb_events files (Dangerous)
	5) binaries:	Remove cb_module binary files (VERY Dangerous)
	6) logs:	Clear logs after a specified amount of days
	7) status:	Show the current status of the CB partitions
	8) restart:	Reboot the Carbon Black service.
	all)		Perform all options, sequentially
	exit)		Exit the program
EOF
printf "Please enter your choice below: ([1-8]/all): "
read action

case $action in
	1)
		hprof
		;;
	2)
		cbdiag
		;;
	3)
		clean_yum
		;;
	4)
		check_modulestore
		;;
	5)
		clear_cb_module_binary
		;;
	6)
		clear_var-log-cb-datastore
		;;
	7)
		status_show
		;;
	8)
		reboot_cb
		;;
	all)
		hprof
		cbdiag
		clean_yum
		check_modulestore
		clear_cb_module_binary
		clear_var-log-cb-datastore
		reboot_cb
		status_show
		;;
	exit)
		return 0
		;;
	*)
		echo "Unknown option, please try again"
		;;
esac

clean_cb	# always return to interactive

}

#####################################

confirm_and_delete () {
	if [ "$confirmation" = y ]
	then
		echo "Deleting $1..."
		rm $1
	elif [ "$confirmation" = n ]
	then
		echo "Cancellation... exiting..."
		return 0
	elif [ "$confirmation" = r ]
	then
		echo $1
	else
		echo "unknown input... exiting..."
		exit 1 
	fi 	
}

#####################################

hprof () {
	echo "Looking for old hprof files..."
	old_hprof=$(find /var/log/cb -type f -iname "*.hprof.old")
	if [ $(echo $old_hprof | wc -w) > 0 ] 
	then
		printf "$(old_hprof | wc -w) files found. Delete? (y/n): "
		read confirmation
		for i in $old_hprof
		do
			confirm_and_delete $i
		done

	else	
		echo "No hprof files."
	fi
}

cbdiag () {
	echo "Looking for old cbdiag files..."
	echo "Warning: Do not delete the cbdiag utility /usr/share/cb/cbdiag (excluded by default)"

	old_cbdiag=$(find / -not -path "/usr/share/cb/*" -type f -iname "cbdiag.*")
	if [ $(echo $old_cbdiag | wc -w) > 0 ] 
	then
		printf "$(old_cbdiag | wc -w) files found. Delete? (y/n): "
		read confirmation
		for i in $old_cbdiag
		do
			confirm_and_delete $i
		done
	else	
		echo "No cbdiag files."
	fi
}

#####################################

clean_yum () {
	echo "Cleaning /var ..."
	yum clean all
	echo "Cleaned /var !"
	sleep 1
	grep "keepcache=0" /etc/yum.conf || echo "Warning! Yum caching is not turned on."
	sleep 1
}

#####################################

check_modulestore () {
	du -h /var/cb/data/solr --max-depth=1 | awk '/events/||/modules/ { print $2 " is " $1 }'

	printf "Would you like to purge cbmodules? (y/n): "
	read confirmation
	[ $confirmation = y ] && purge_cbmodule_data

	printf "Would you like to purge cbevents? (y/n): "
	read confirmation
	[ $confirmation = y ] && purge_cbevents
}

purge_cbmodule_data () {
	max_days=$(grep "MaxEventStoreDays=" /etc/cb/cb.conf | sed -n -e 's/MaxEventStoreDays\=//p')
	echo "Warning: This will irrecoverably remove data from the Cb Response server. After removing this data, you will receive a 404 page when attempting to view binary details pages older than MaxEventStoreDays and this data will no longer appear in the Binary Search"
	printf "You MaxEventStoreDays is $max_days. Do you wish to delete modules before that period? (y/n): "
	read confirmation

	if [ $confirmation = y ]
	then
		curl http://127.0.0.1:8080/solr/cbmodules/update?commit=true -H "Content-Type: text/xml" -d "<delete><query>last_seen:[* TO NOW-${max_days}DAYS]</query></delete>"
		echo "Deleted Modules"
	else
		echo "Deletion cancelled."
	fi
}

purge_cbevents() {
	echo "Warning: This will irrecoverably remove data from the CB Response server. After removing this data, users will receive a 404 page when attempting to view Alerts and process data will no longer appear in Process Search"

	available_cores=$(curl "http://127.0.0.1:8080/solr/admin/cores?action=STATUS&wt=json&indexInfo=false&indent=true" 2>/dev/null | grep -E -- 'name|startTime' | sed 's/"//g') || echo "Unable to access server..."
	echo "Cores:"; echo "$available_cores"

	printf "Which would you like to delete? (type "n" to cancel): "
	read chosen_core

	[ ! "$chosen_core" -eq "n" ] || echo "Canceling" && return 1
	grep $chosen_core <<< "$available_cores" || echo "Core not found... exiting" && return 1

	printf "You have chosen to purge $available_cores. Proceed? (y/n): "	
	read confirmation

	if [ "$confirmation" = "y"]
	then
		echo "Unloading core..."
		curl "http://localhost:8080/solr/admin/cores?action=UNLOAD&core=$chosen_core"
		echo "Pretend unloaded core"
		curl "http://localhost:8080/solr/admin/cores?action=STATUS&indexInfo=true&indent=true&wt=json"  | grep $chosen_core || Error.. core not unloaded.. exiting && return 1
		rm -rf /var/cb/data/solr/cbevents/$chosen_core
		echo "Deleting $chosen_core"
	else
		echo "Cancelling..."
	fi

}

#####################################

clear_var-log-cb-datastore (){
printf "Please enter the the amount of days you wish to keep: "
read days

var_log_files=$(find /var/log/cb/datastore -type f -mtime +$days)

printf "Found $(echo $var_log_files | wc -w) logs past $days days. They will be deleted, are you sure? (y/n): "
read confirmation
for i in $var_log_files
do
	confirm_and_delete $i
done
}

#####################################

clear_cb_module_binary () {
	max_days=$(grep "MaxEventStoreDays=" /etc/cb/cb.conf | sed -n -e 's/MaxEventStoreDays\=//p')

	echo "This will irrecoverably remove data from the CB Response server. After removing this data, attempting to download this binary from the binary details page will receive a 404."; echo

	printf "How many days of binaries would you like to keep? (CB's MaxEventStoreDays is $max_days): "
	read days
	[[ $days == "" ]] && echo "Error, days not entered... Cancelling..." && return 1
	cleared_binary=$(find /var/cb/data/modulestore -name "*.zip" -type f -mtime +$days)

	printf "This will delete ALL binaries past $days days. Are you sure you want to do this? (y/n): "
	read confirmation
	for i in $cleared_binary
	do
		confirm_and_delete $i
	done 
}

#####################################

status_show () {
	df -h | awk '/var\/log\/cb/||/var\/cb\/data/ { print $6 " is at " $5 " usage. (using " $3 ")"}'	# shows the disk usage of the partitions mounted on /var/log/cb and /var/cb/data
}

#####################################

reboot_cb () {
	echo "Rebooting carbon black..."
	/usr/share/cb/cbservice cb-enterprise restart
	echo "Rebooted!"
}

#####################################

check_for_cb () {
	if [ -f /tmp/cb.status ] && [[ $(cat /tmp/cb.status) == "installed" ]]
	then
		return 0
	elif yum list installed 2>/dev/null | grep -q cb-enterprise
	then
		echo "Checking if Carbon Black Enterprise Server is installed..."
		sleep 1
		echo "installed" > /tmp/cb.status
		return 0
	else
		echo "Checking if Carbon Black Enterprise Server is installed..."
		sleep 1
		echo && echo "Carbon Black does not seem to be installed. Exiting!"
		exit 1
	fi
}

############# SCRIPT ################

[ $# -gt 0 ] || usage			# show usage if no parameters are given

case $@ in				# take parameters

	-s)
		check_for_cb		# check for cb install
		copyright_notice	# show copyright license
		status_show		# show status of /var/log/cb and /var/cb/data
		exit $?
		;;
	-d)
		check_for_cb		# check for cb install
		copyright_notice	# show copyright license
		check_root		# confirm that we are running with root privileges
		warning			# show warning textbox
		clean_cb		# run clean_cb, the main interactive
		exit $?
		;;
	-c)
		less LICENSE || echo "You can only view the full license by cloning https://gitlab.com/tomasbulk/cb-troubleshoot.git"	# display the license, must be in the same folder as the command
		exit
		;;
	-h)
		usage			# display usage
		;;
	*)	
		usage			# display usage if unknown parameters are given
		;;

	esac
