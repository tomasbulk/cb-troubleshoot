# CB-Troubleshoot
![Carbon Black Logo](image.jpg)

Simple Bash script for removing excess storage from Carbon Black EDR.
Paired with a simple optional alert system.

### Who this is designed for
This script is designed for use with **Premier Network Solutions.** Anyone else may attempt to use it, but there are *no guarantees it will work whatsoever.*

### Download
To download, simply clone the repository.
```bash
git clone https://gitlab.com/tomasbulk/cb-troubleshoot.git
cd cb-troubleshoot
```

### Configure
You must configure alert.conf before installing. This setups up the variables for email alerts.
```bash
system="$(cat /etc/hostname)"
subject="Carbon Black Storage Alert!"
to="user1@example.com"
also_to="user2@example.com"
server="smtp://smtp.example.com"
```


### Install
To install on a Carbon Black Server, run the following commands as root.
```bash
make
make install
```
cb-troubleshoot installs to `/usr/local` by default. Please ensure it is in your path.

### Uninstall
To uninstall, simply run:
```bash
make uninstall
```

### Using the alert system
After installing, `cb-alert` checks for high disk usage and sends an email alert. This will need to be configured to run on a schedule. Possible solutions include:
- cron
- systemd

### Usage
```bash
user@system cb-troubleshoot -h
	Options:
	-s	Check the status of the CB partitions
	-d	Clean the CB partitions (Dangerous)
	-c	Copyright Notice
	-h 	Print this dialogue

user@system % cb-troubleshoot -d
Please determine how you would like to clean:
        1) hprof:       Remove old hprof files.
        2) cbdiag:      Remove old cbdiag files.
        3) clean-var:   Clean /var directory with "yum clean all"
        4) modules:     Remove cb_module files and cb_events files (Dangerous)
        5) binaries:    Remove cb_module binary files (VERY Dangerous)
        6) logs:        Clear logs after a specified amount of days
        7) status:      Show the current status of the CB partitions
        8) restart:     Reboot the Carbon Black service.
        all)            Perform all options, sequentially
        exit)           Exit the program
```
