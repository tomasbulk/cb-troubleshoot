#!/bin/bash

# check for the config file
[[ -f "/usr/local/etc/cb-troubleshoot/alert.conf" ]] || {
	# exit
	echo "Please include an alert.conf file in /usr/local/etc/cb-troubleshoot/"
	exit 1
}

# source the config file
source alert.conf

# date and storage variables
date="$(date '+DATE: %D%nTIME: %T%n')"
storage="$(/usr/local/bin/cb-troubleshoot -s | tail -n 2)"

# email content
content = "$(cat << EOF
Warning, one of the Carbon Black paritions is running out of space!

$date
$storage

You can view a daily query of storage use at /var/log/cb/cb-troubleshoot-alert.log

This is an automated email from $system
EOF)" 

# log the current date and storage
echo -e "$date\n$storage\n\n" >> /var/log/cb/cb-troubleshoot-alert.log

# check if the storage is 95+%
[[ grep -E "100%|9[5-9]%" <<< $storage ]] && {
# send the email
	echo $content | \
		mailx -v -s "$system - $subject" -S smtp="$server" -c "$also_to" "$to"
}

exit 0
